import React, {Component} from 'react';
import {Row, Col, Tab, Tabs} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import Card from "../../App/components/MainCard";

class SamplePage extends Component {
    
    

    render() {
        console.log(this.props)
        return (
            <Aux>
                <Row>
                    <Col>
                        <h5 className="mt-4">Bem vindo ao EventUSP</h5>
                            <hr/>
                            <Tabs variant="pills" defaultActiveKey="home" className="mb-3">
                                <Tab eventKey="home" title="HOME">
                                    <p>Um sistema de gerenciamento de eventos universitários! Desde suas aulas cotidianas, até as festas e os eventos de extensão e cultura! Tudo o que você precisa estar de olho para aproveitar ao máximo seu ambiente universitário!</p>
                                </Tab>
                                <Tab eventKey="about" title="SOBRE">
                                    <p>Somos um grupo de estudantes da Universidade de São Paulo e fizemos este projeto para o Interhacks2019.</p>
                                </Tab>
                            </Tabs>
                    </Col>
                </Row>
           </Aux>
        );
    }
}

export default SamplePage;