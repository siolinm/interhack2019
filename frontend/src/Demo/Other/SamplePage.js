import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import Card from "../../App/components/MainCard";

class SamplePage extends Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card title='Bem vindo ao (...)' isOption>
                            <p>
                                Bem vindo ao seu gerenciador de eventos universitários!
                                Tudo o que você precisa em uma só plataforma.
                                Gerencie seus eventos e descobra novas oportunidades!
                            </p>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default SamplePage;