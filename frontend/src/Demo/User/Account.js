import React, { Component } from 'react';
import { Row, Col, Card, Table} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

import avatar1 from '../../assets/images/user/avatar-1.jpg';
import avatar2 from '../../assets/images/user/avatar-2.jpg';
import avatar3 from '../../assets/images/user/avatar-3.jpg';

class Account extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <h5>Nome: {localStorage.getItem("name")}</h5>
                            </Card.Header>
                            <Card.Body>
                                <strong>RG: {localStorage.getItem("rg")}</strong>
                                <p>Tipo: {localStorage.getItem("tipo")}</p>
                                <p>NUSP: {localStorage.getItem("nusp")}</p>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default Account;