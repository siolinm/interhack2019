import React from 'react';
import {NavLink} from 'react-router-dom';

import './../../../assets/scss/style.scss';
import api from '../../../App/services/api.js'
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";



class SignOut extends React.Component {

    logout() {
        localStorage.clear();
        window.location.href = '/';
    }

    render(){
            return this.logout()
    }



}

export default SignOut;