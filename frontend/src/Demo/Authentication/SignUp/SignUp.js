import React from 'react';
import api from '../../../App/services/api.js'
import {NavLink} from 'react-router-dom';
import {Form} from 'react-bootstrap';


import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          name: "",
          password: "",
          nUsp: "",
          tipo: "Aluno",
          rg: ""        
        };

        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeNusp = this.handleChangeNusp.bind(this);
        this.handleChangeTipo = this.handleChangeTipo.bind(this);
        this.handleChangeRg = this.handleChangeRg.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeRg(event) {
        this.setState({rg: event.target.value});
    }

    handleChangeName(event) {
        this.setState({name: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    handleChangeNusp(event) {
        this.setState({nUsp: event.target.value});
    }

    handleChangeTipo(event) {
        this.setState({tipo: event.target.value});
    }

    async handleSubmit(event){        
        event.preventDefault()
        const response = await api.post('/user', { rg : this.state.rg, password : this.state.password, name: this.state.name, nUsp: this.state.nUsp, tipo: this.state.tipo}) 
        
        this.props.history.push('/signin')
        
    }
    

    render () {
        return(
            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-user-plus auth-icon"/>
                                </div>
                                <h3 className="mb-4">Registro</h3>
                                <form onSubmit = {this.handleSubmit}>
                                    <div className="input-group mb-3">
                                        <input type="text" value = {this.state.name} onChange = {this.handleChangeName} className="form-control" placeholder="Nome"/>
                                    </div>
                                    <div className="input-group mb-3">
                                        <input type="text" value = {this.state.rg} onChange = {this.handleChangeRg} className="form-control" placeholder="RG"/>
                                    </div>
                                    <div className="input-group mb-3">
                                        <input type="text" value = {this.state.nUsp} onChange = {this.handleChangeNusp} className="form-control" placeholder="NUSP"/>
                                    </div>
                                    <div className="input-group mb-4">
                                        <input type="password" value = {this.state.password} onChange = {this.handleChangePassword} className="form-control" placeholder="Senha"/>
                                    </div>
                                    <div className="input-group mb-4">
                                        <Form.Control as="select" value = {this.state.tipo} onChange = {this.handleChangeTipo}>
                                            <option>Aluno</option>
                                            <option>Professor</option>
                                            <option>Entidade</option>
                                        </Form.Control>
                                    </div>
                                    <button type = "submit" className="btn btn-primary shadow-2 mb-4">Registre-se</button>                                
                                </form>
                                <p className="mb-0 text-muted">Já tem uma conta? <NavLink to="/signin">Login</NavLink></p>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}

export default SignUp;