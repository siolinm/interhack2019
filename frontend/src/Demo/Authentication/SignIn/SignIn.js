import React from 'react';
import {NavLink} from 'react-router-dom';

import './../../../assets/scss/style.scss';
import api from '../../../App/services/api.js'
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";



class SignIn extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
          rg: "",
          password: ""
        };

        this.handleChangeRg = this.handleChangeRg.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeRg(event) {
        this.setState({rg: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    async handleSubmit(event){        
        event.preventDefault()
        const response = await api.post('/login', { rg : this.state.rg, password : this.state.password})        
                        
        // const { _id} = response.data
        if(response){

            localStorage.setItem('rg', response.data.rg)
            localStorage.setItem('name', response.data.name)
            localStorage.setItem('tipo', response.data.tipo)
            localStorage.setItem('nusp', response.data.nUsp)
        }
        this.props.history.push('/eventos', response.data)
    }
    
    
    render () {               
        
        return(

            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <form onSubmit = {this.handleSubmit}>
                                <div className="card-body text-center">
                                    <div className="mb-4">
                                        <i className="feather icon-unlock auth-icon"/>
                                    </div>
                                    <h2 className = "mb-4">EventUSP</h2>
                                    <h3 className="mb-4">Login</h3>                                    
                                    <div className="input-group mb-3">
                                        <input type="text" value = {this.state.rg} className="form-control" onChange = {this.handleChangeRg} placeholder="RG"/>
                                    </div>
                                    <div className="input-group mb-4">
                                        <input type="password" value = {this.state.password} onChange = {this.handleChangePassword} className="form-control" placeholder="Senha"/>
                                    </div>
                                    <button type = "submit" className="btn btn-primary shadow-2 mb-4">Login</button>
                                    <p className="mb-0 text-muted">Não tem uma conta? <NavLink to="/signup">Registre-se</NavLink></p>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </Aux>
        ); 
    }
}

export default SignIn;