import React from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import api from '../../App/services/api.js'


import Aux from "../../hoc/_Aux";

class BootstrapTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            userRg: "",
            tipo: "Aula",
            local: "",
            date: "",
            startTime: "",
            endTime: "",
            campus: "",
            institute: "",
            spaces: 0
        };

        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.handleChangeTipo = this.handleChangeTipo.bind(this);
        this.handleChangeLocal = this.handleChangeLocal.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeStartTime = this.handleChangeStartTime.bind(this);
        this.handleChangeEndTime = this.handleChangeEndTime.bind(this);
        this.handleChangeCampus = this.handleChangeCampus.bind(this);
        this.handleChangeInstitute = this.handleChangeInstitute.bind(this);
        this.handleChangeSpaces = this.handleChangeSpaces.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        
    }
    
    handleChangeName(event) {
        this.setState({name: event.target.value});
    }

    handleChangeDescription(event) {
        this.setState({description: event.target.value});
    }

    handleChangeTipo(event) {
        this.setState({tipo: event.target.value});
    }

    handleChangeLocal(event) {
        this.setState({local: event.target.value});
    }

    handleChangeDate(event) {
        this.setState({date: event.target.value});
    }
    
    handleChangeStartTime(event) {
        this.setState({startTime: event.target.value});
    }
    
    handleChangeEndTime(event) {
        this.setState({endTime: event.target.value});
    }
    
    handleChangeCampus(event) {
        this.setState({campus: event.target.value});
    }
    
    handleChangeInstitute(event) {
        this.setState({institute: event.target.value});
    }
    
    handleChangeSpaces(event) {
        this.setState({spaces: event.target.value});
    }
    
    async handleSubmit(event){        
        event.preventDefault()
        const response = await api.post('/event', 
        {name: this.state.name,
        description: this.state.description,
        userRg: localStorage.getItem('rg'),
        tipo: this.state.tipo,
        local: this.state.local,
        date: this.state.date,
        startTime: this.state.startTime,
        endTime: this.state.endTime,
        campus: this.state.campus,
        institute: this.state.institute,
        spaces: this.state.spaces})      
        this.props.history.push('/hello')
    }
    
    render() {
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h3">Criar novo evento</Card.Title>
                                <Card.Subtitle><em>Somente usuários com permissão</em></Card.Subtitle>
                            </Card.Header>
                            <Card.Body>
                                <form onSubmit = {this.handleSubmit}>
                                    <Row>
                                        <Col md={6}>
                                            <Form>
                                                <Form.Group controlId="exampleForm.ControlInput1">
                                                    <Form.Label>Nome do Evento</Form.Label>
                                                    <Form.Control type="email" value = {this.state.name} onChange = {this.handleChangeName} placeholder="Nome" />
                                                </Form.Group>
                                                <Form.Group controlId="exampleForm.ControlSelect1">
                                                    <Form.Label>Tipo:</Form.Label>
                                                    <Form.Control as="select" value = {this.state.tipo} onChange = {this.handleChangeTipo}>
                                                        <option>Aula</option>
                                                        <option>Monitoria</option>
                                                        <option>Palestra</option>
                                                        <option>Extensão</option>
                                                        <option>Festa</option>
                                                        <option>Ensaio/Reunião</option>
                                                    </Form.Control>
                                                </Form.Group>
                                                <Form.Group controlId="exempleForm.ControlInput1">
                                                    <Form.Label>Campus:</Form.Label>
                                                    <Form.Control type="sm" value = {this.state.campus} onChange = {this.handleChangeCampus} placeholder="exemplo: Butantã" className="mb-3" />
                                                </Form.Group>
                                                <Form.Group controlId="exempleForm.ControlInput1">
                                                    <Form.Label>Instituto:</Form.Label>
                                                    <Form.Control type="sm" value = {this.state.institute} onChange = {this.handleChangeInstitute} placeholder="exemplo: IME-USP" className="mb-3" />
                                                </Form.Group>
                                                <Form.Group controlId="exempleForm.ControlInput1">
                                                    <Form.Label>Local:</Form.Label>
                                                    <Form.Control type="sm" value = {this.state.local} onChange = {this.handleChangeLocal} placeholder="exemplo: Bloco B Sala 5" className="mb-3" />
                                                </Form.Group>
                                            </Form>

                                        </Col>
                                        <Col md={6}>
                                            <Form>
                                                <Form.Group controlId="exempleForm.ControlInput1">
                                                    <Form.Label>Data:</Form.Label>
                                                    <Form.Control type="sm" value = {this.state.date} onChange = {this.handleChangeDate} placeholder="dd/mm/aa" className="mb-3" />
                                                </Form.Group>

                                                <Form.Group controlId="exempleForm.ControlInput1">
                                                    <Form.Label>Horário de início:</Form.Label>
                                                    <Form.Control type="sm" value = {this.state.startTime} onChange = {this.handleChangeStartTime} placeholder="hh:mm" className="mb-3" />
                                                </Form.Group>

                                                <Form.Group controlId="exempleForm.ControlInput1">
                                                    <Form.Label>Horário de término:</Form.Label>
                                                    <Form.Control type="sm" value = {this.state.endTime} onChange = {this.handleChangeEndTime} placeholder="hh:mm" className="mb-3" />
                                                    <Form.Text type="sm" className="text-muted">Por favor, use horários entre 00:00 e 23:59</Form.Text>
                                                </Form.Group>

                                                <Form.Group controlId="exempleForm.ControlInput1">
                                                    <Form.Label>Total de vagas:</Form.Label>
                                                    <Form.Control type="sm" value = {this.state.spaces} onChange = {this.handleChangeSpaces} className="mb-3" />
                                                </Form.Group>
                                            </Form>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form>
                                                <Form.Group controlId="exampleForm.ControlTextarea1">
                                                    <Form.Label>Descrição do evento:</Form.Label>
                                                    <Form.Control value = {this.state.description} onChange = {this.handleChangeDescription} as="textarea" rows="5" />
                                                </Form.Group>
                                            </Form>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={6}>
                                            <Button  type="submit" variant="primary">ENVIAR</Button>
                                        </Col>
                                    </Row>
                                </form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux >
        );
    }
}

export default BootstrapTable;