import React, { Component } from 'react';
import { Row, Col, Card } from 'react-bootstrap';

import api from '../../App/services/api.js'
import Aux from "../../hoc/_Aux";

class Info extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id : 0, 
            event : {},
            user : {}}
        }
    
    async loadUser(){
        let _id = this.props.location.state.data
        const eve = await api.get(`/detail/${_id}`)
        const users = await api.get(`/listusers/${_id}`)
        //console.log(user)
        //const user = await api.get(`/getuser/${eve.data.userRg}`)
        this.setState({id:_id, event: eve.data})
        this.setState({user : users})
        console.log(this.state.user)
        // await Promise.all(this.state.event.participants.map( _id =>{
        //     user.push(api.get(`/getuser/${_id}`))}
        // ))

        // console.log(this.state.user)
    }

    componentDidMount() {
        this.loadUser()       
    }  
    render() {
        return (
            <Aux>
                <Row>
                    <Col md={6} xl={4}>
                        <Card>
                            <Card.Header>
                                <h5>Nome: {this.state.event.name}</h5>
                            </Card.Header>
                        </Card>
                        <Card>
                            <Card.Header>
                                <h5>RG do Organizador:</h5>
                            </Card.Header>
                            <Card.Body>

                                <div className="row d-flex align-items-center">
                                    <div className="col-9">
                                        <h5 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-users text-c-green f-30 m-r-5" />{this.state.event.userRg}</h5>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xl={3}>
                        <Card>
                            <Card.Header>
                                <h5>Data e Horário</h5>
                            </Card.Header>
                            <Card.Body>
                                <div className="row d-flex align-items-center">
                                    <div className="col-9">
                                        <h5 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-calendar text-c-green f-30 m-r-5" />{this.state.event.date}</h5>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div className="row d-flex align-items-center">
                                    <div className="col-9">
                                        <h5 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-watch text-c-green f-30 m-r-5" />Das <mark>{this.state.event.startTime}</mark> até <mark>{this.state.event.endTime}</mark></h5>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} xl={4}>
                        <Card>
                            <Card.Header>
                                <h5>Localização: </h5>
                            </Card.Header>
                            <Card.Body>
                                <div className="row d-flex align-items-center">
                                    <div className="col-9">
                                        <h5 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-map-pin text-c-green f-30 m-r-5" />{this.state.event.local}</h5>
                                    </div>
                                </div>
                                <br />
                                <div className="row d-flex align-items-center">
                                    <div className="col-9">
                                        <h5 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-sun text-c-green f-30 m-r-5" />{this.state.event.campus}</h5>
                                    </div>
                                </div>
                                <br />
                                <div className="row d-flex align-items-center">
                                    <div className="col-9">
                                        <h5 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-home text-c-green f-30 m-r-5" />{this.state.event.institute}</h5>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <h5>Descrição:</h5>
                            </Card.Header>
                            <Card.Body>
                                <p>{this.state.event.description}</p>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <h5>Participantes confirmados:</h5>
                            </Card.Header>
                            {this.state.user.data && this.state.user.data.map(p => {
                                return(
                                 <Card.Body>
                                 <div className="row d-flex align-items-center">
                                     <div className="col-9">
                                         <h5 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-user text-c-green f-30 m-r-5" />{p.name}</h5>
                                     </div>
                                 </div>
                                 <br />
                             </Card.Body>
                            )})}
                            
                        </Card>
                    </Col>
                </Row>
            </Aux >
        );
    }
}

export default Info;