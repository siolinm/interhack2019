import React from 'react';
import { Row, Col, Card, Table, Form, Button } from 'react-bootstrap';
import api from '../../App/services/api.js'

import Aux from "../../hoc/_Aux";
import DEMO from "../../store/constant";

import avatar1 from '../../assets/images/user/avatar-1.jpg';
import avatar2 from '../../assets/images/user/avatar-2.jpg';
import avatar3 from '../../assets/images/user/avatar-3.jpg';

class BootstrapTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventos: []
        };
    }

    async loadEvents() {        
        const resp = await api.get("/listar")
        this.setState({eventos : resp.data})
        console.log(resp.data)
    }

    componentDidMount() {
        this.setState({ eventos: [] })                
        this.loadEvents()
    }          
    
    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card className='All-Events'>
                            <Card.Header>
                                <Card.Title as='h2'>Eventos ativos</Card.Title>
                            </Card.Header>
                            <Card.Body className='px-0 py-2'>
                                <Table responsive hover>
                                    <tbody>
                                        {this.state.eventos.map(e => {
                                            return (
                                            <tr className="unread">
                                                <td><img className="circle" style={{ width: '40px' }} src={avatar1} alt="activity-user" /></td>
                                                <td>
                                                    <h6 className="mb-1">{e.name}</h6>
                                                    <p className="m-0">{e.description.substr(0, 50)}...</p>
                                                </td>
                                                <td>
                                                    <h6 className="text-muted">{e.date}</h6>
                                                </td>
                                                <td>
                                                    <h6 className="text-muted"><i className="fa fa-circle text-c-green f-10 m-r-15" />{`${e.startTime} até ${e.endTime}`}</h6>
                                                </td>
                                                <td><a href={"/info"} className="label theme-bg2 text-white f-12">Infos</a><a href={DEMO.BLANK_LINK} className="label theme-bg text-white f-12">Participar</a></td>
                                            </tr>)
                                        })}                                 
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default BootstrapTable;