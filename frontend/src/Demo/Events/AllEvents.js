import React from 'react';
import { Row, Col, Card, Table, Form, Button } from 'react-bootstrap';
import api from '../../App/services/api.js'

import Aux from "../../hoc/_Aux";
import DEMO from "../../store/constant";

import avatar1 from '../../assets/images/user/avatar-1.jpg';
import avatar2 from '../../assets/images/user/avatar-2.jpg';
import avatar3 from '../../assets/images/user/avatar-3.jpg';
import avatar4 from '../../assets/images/user/avatar-4.jpg';
import avatar5 from '../../assets/images/user/avatar-5.jpg';



class BootstrapTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventos: [],
            filtered: [],
            tipo : "Aula",
            string : ""
        };

        this.handleClick = this.handleClick.bind(this);
        this.onChangeHandleString = this.onChangeHandleString.bind(this);
        this.onChangeHandleTipo = this.onChangeHandleTipo.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handlerInfo = this.handlerInfo.bind(this);
    }

    async loadEvents() {        
        const resp = await api.get("/listar")
        this.setState({eventos : resp.data, filtered: resp.data})
    }

    async handleClick(event){
        const resp = await api.post("/updateparticipants", {_id : event.target.attributes.value.value, rg : localStorage.getItem("rg")})
        if(resp)
            alert("Chek-in realizado com sucesso")
    }

    onChangeHandleString(event) {
        this.setState({string: event.target.value});
    }   

    handlerInfo(event) {
        const resp = event.target.attributes.value.value
        this.props.history.push({pathname: '/info', state: {data:resp}})
    }
    

    onChangeHandleTipo(event) {
        this.setState({tipo: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        if(this.state.string === ""){
            this.setState({filtered : this.state.eventos});
            return 
        }
        let aux = this.state.eventos.filter(e => {
            if(this.state.tipo === "Instituto"){
                return e.institute.includes(this.state.string)
            }
            if(this.state.tipo === "Nome"){
                return e.name.includes(this.state.string)
            }
            
            return e.tipo === this.state.tipo && e.name.includes(this.state.string)
        })
        this.setState({filtered: aux});
    }

    componentDidMount() {
        this.setState({ eventos: [] })                
        this.loadEvents()
    }          


    
    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Body>
                                <form onSubmit={this.handleSubmit}>
                                    <Row>
                                        <Col md={4}>
                                            <Card.Title as="h6">Pesquise seu evento:</Card.Title>
                                            <Form.Control type="sm"  value={this.state.string} onChange={this.onChangeHandleString} placeholder="Interhacks2019" className="mb-3" />
                                        </Col>
                                        <Col md={4}>
                                            <Form.Group controlId="exampleForm.ControlSelect1">
                                                <Form.Label>Busca por:</Form.Label>
                                                <Form.Control as="select" value = {this.state.tipo} onChange={this.onChangeHandleTipo}>
                                                    <option>Aula</option>
                                                    <option>Nome</option>
                                                    <option>Instituto</option>
                                                    <option>Festa</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Button type="submit" variant="primary">Buscar</Button>
                                    </Row>
                                </form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card className='All-Events'>
                            <Card.Header>
                                <Card.Title as='h2'>Próximos eventos</Card.Title>
                            </Card.Header>
                            <Card.Body className='px-0 py-2'>
                                <Table responsive hover>
                                    <tbody>
                                        {this.state.filtered.map(e => {
                                            return (
                                            <tr className="unread">
                                                {e.tipo == "Aula" && <td><img className="circle" style={{ width: '40px' }} src={avatar1} alt="activity-user" /></td>}
                                                {e.tipo == "Monitoria" && <td><img className="circle" style={{ width: '40px' }} src={avatar2} alt="activity-user" /></td>}
                                                {e.tipo == "Palestra" && <td><img className="circle" style={{ width: '40px' }} src={avatar1} alt="activity-user" /></td>}
                                                {e.tipo == "Extensão" && <td><img className="circle" style={{ width: '40px' }} src={avatar5} alt="activity-user" /></td>}
                                                {e.tipo == "Festa" && <td><img className="circle" style={{ width: '40px' }} src={avatar4} alt="activity-user" /></td>}
                                                {e.tipo == "Ensaio/Reunião" && <td><img className="circle" style={{ width: '40px' }} src={avatar3} alt="activity-user" /></td>}
                                                <td>
                                                    <h6 className="mb-1">{e.name}</h6>
                                                    <p className="m-0">{e.description.substr(0, 50)}...</p>
                                                </td>
                                                <td>
                                                    <h6 className="text-muted">{e.date}</h6>
                                                </td>
                                                <td>
                                                    <h6 className="text-muted"><i className="fa fa-circle text-c-green f-10 m-r-15" />{`${e.startTime} até ${e.endTime}`}</h6>
                                                </td>
                                                <td><a href={"#"} value={e._id} onClick = {this.handlerInfo} className="label theme-bg2 text-white f-12">Infos</a><a href="#" value = {e._id} onClick={this.handleClick} className="label theme-bg text-white f-12">Participar</a></td>
                                            </tr>)
                                        })}                                 
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default BootstrapTable;