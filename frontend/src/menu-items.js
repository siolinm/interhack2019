export default {
    items: [
        {
            id: 'hello',
            title: '',
            type: 'group',
            icon: 'feather icon-cloud',
            children: [
                {
                    id: 'hello',
                    title: 'Bem vindo',
                    type: 'item',
                    url: '/hello',
                    icon: 'feather icon-cloud',
                },
            ]
        },
        {
            id: 'user',
            title: 'Usuário',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'login',
                    title: 'Login',
                    type: 'item',
                    url: '/signin',
                    icon: 'feather icon-log-in',
                },
                {
                    id: 'account',
                    title: 'Conta',
                    type: 'item',
                    url: '/conta',
                    icon: 'feather icon-home',
                },
                {
                    id: 'logout',
                    title: 'Logout',
                    type: 'item',
                    url: '/signout',
                    icon: 'feather icon-log-out',
                }
            ]
        },
        {
            id: 'events',
            title: 'Eventos',
            type: 'group',
            icon: 'feather icon-home',
            children: [
                {
                    id: 'all-events',
                    title: 'Todos',
                    type: 'item',
                    url: '/eventos',
                    icon: 'feather icon-check-square'
                },
                {
                    id: 'creat-events',
                    title: 'Criar',
                    type: 'item',
                    url: '/novoevento',
                    icon: 'feather icon-edit'
                },
                {
                    id: 'last-events',
                    title: 'Últimos',
                    type: 'item',
                    url: '/ultimos',
                    icon: 'feather icon-bookmark'
                }
            ]
        },
    ]
}