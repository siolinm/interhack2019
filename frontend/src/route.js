import React from 'react';

const SignUp1 = React.lazy(() => import('./Demo/Authentication/SignUp/SignUp'));
const Signin1 = React.lazy(() => import('./Demo/Authentication/SignIn/SignIn'));

const route = [
    { path: '/signup', exact: true, name: 'Signup 1', component: SignUp1 },
    { path: '/signin', exact: true, name: 'Signin 1', component: Signin1 }
];

export default route;