
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const routes = require('./routes.js')

const app = express()

mongoose.connect('mongodb+srv://dllm:interhack@interhack-5hrfh.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true   
})

mongoose.Promise = global.Promise;

app.use(cors())
app.use(express.json())
app.use(routes)

app.listen(8080)

