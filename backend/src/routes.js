const express = require('express')
const UserController = require('./controllers/User.js')
const EventController = require('./controllers/Event.js')

const routes = express.Router()

routes.get('/index', (req, res)=>{res.send("Hello World")})

routes.post('/user', UserController.store)
routes.post('/login', UserController.login)
routes.post('/getuser/:_id', UserController.getUser)

routes.post('/event', EventController.store)
routes.get('/listar', EventController.events)
routes.get('/remove/:id', EventController.removeEvent)
routes.post('/updateparticipants', EventController.updateParticipants)
routes.get('/detail/:id', EventController.eventDetail)
routes.get('/listusers/:id', UserController.getUsersEvent)

module.exports = routes