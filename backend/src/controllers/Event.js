const Evento = require('../models/Event.js')
const UserController = require('./User.js')

module.exports = {

    async store(request, response) {
        const { name, description, userRg, local, tipo, startTime, endTime, date, campus, institute, spaces} = request.body;

        const evento = await Evento.create({
            name, description, userRg, tipo, local, startTime, endTime, date, campus, institute, spaces, participants : []
        })
        return response.json(evento);

    },

    async events(request, response){

        const events = await Evento.find();   
        return response.json(events);
    },

    async removeEvent(req, res){
        const {_id} = req.params;
        const evento = await Evento.findOne({_id})
        const event = await Evento.deleteOne(evento)

        return true
    },

    async updateParticipants(req, res){
      const {_id, rg} = req.body;      
      const evento = await Evento.findOne({_id});
      const user = await UserController.getUser(rg)
      if(evento.participants.includes(user._id))
        return res.json(true)
      evento.participants.push(user._id);
      console.log(evento)
      evento.save()
      return res.json(true)
    },

    async eventDetail(req, res){
        const {id} = req.params;
        const event = await Evento.findOne({_id : id});
        console.log(event)
        return res.json(event);
    }
}