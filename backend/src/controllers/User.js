const User = require('../models/User.js')
const Event = require('../models/Event.js')

module.exports = {

    async store(request, response) {
        const {name, rg, nUsp, password, tipo} = request.body;

        const userExists = await User.findOne({ nUsp : nUsp});
        const userExists2 = await User.findOne({ rg });

        if(userExists || userExists2){
            return false;
        }
        const user = await User.create({
            name,
            password,
            nUsp,
            tipo,
            rg,
            numEvents : 0
        })
        return response.json(user)

    },

    async login(req, res){
        const { rg, password} = req.body

        const userExists = await User.findOne({rg, password});
        
        if(userExists){
            console.log(userExists)            
            return res.json(userExists);
        }
        console.log('Shoraste?')
        return res.json(false);
    },

    async getUser(rg){
        const user = await User.findOne({rg})
        console.log(user)
        return user
    },

    async getUsersEvent(req, res){
        const {id} = req.params
        const event = await Event.findOne({_id : id})
        const user = await User.find({
            $and : [
                {_id : {$in: event.participants}}
            ]
        })
        console.log(user)
        return res.json(user)
    }
}