const { Schema, model } = require('mongoose')

const UserSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    nUsp : Number,
    password: {
        type : String,
        required : true
    },
    tipo : {
        type : String,
        required : true
    },
    rg : {
        type : String,
        required : true
    },
    numEvents : {
        type : Number,
        required : true
    }
},{ 
    timestamps: true
})

module.exports = model('User', UserSchema)