const { Schema, model } = require('mongoose')

const EventSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    description : {
        type : String,
        required : true
    },
    userRg : {
        type : String,
        required : true
    },
    local : {
        type : String,
        required : true
    },
    tipo : {
        type : String, /*Date?*/
        required : true 
    },
    startTime : {
        type : String, /*Date?*/
        required : true 
    },
    endTime : {
        type : String, /*Date?*/
        required : true
    },
    date:{
        type : String,
        required : true
    },
    campus : {
        type : String,
        required : true
    },
    institute : {
        type : String,
        required : true
    },
    spaces : {
        type : Number,
        required : true
    },
    participants : [{
        type : Schema.Types.ObjectId,
        ref : 'User'
    }]        
})

module.exports = model('Event', EventSchema)
